<!doctype html>
<html lang="en">

<head>
    <title>AJAX CRUD</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <Script src="{{asset('js/jquery-3.6.3.min.js')}}" ></Script>
    
    <!-- Bootstrap CSS v5.2.1 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">



</head>

<body>

    <a class="btn btn-dark" href="{{ route('index') }}">View Products</a>

    <form action="" id="frm">
        @csrf
        <table>
            <tr>
                <td>Product Name:</td>
                <td><input type="text" name="name"></td>
            </tr>

        </table>
        <button type="submit" id="submit">Submit</button>
    </form>
    <p id="message"></p>

    

    <script>

        $('#frm').submit(function(e) {
            e.preventDefault();
            $('#submit').attr('disabled',true);           
            $.ajax({
                    url: "{{ route('insert-data') }}",
                    data: $('#frm').serialize(),
                    type: 'post',   
                    success: function(result) {
                        $('#message').html(result.result);
                        $('#frm')['0'].reset();
                        $('#submit').attr('disabled',false); 
                    }
                }

            );
        })
    </script>
</body>

</html>
