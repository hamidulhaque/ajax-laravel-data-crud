<!doctype html>
<html lang="en">

<head>
    <title>AJAX CRUD</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
        integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Bootstrap CSS v5.2.1 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
        integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>


</head>

<body>
    <a class="btn btn-light mt-5 text-center" href="{{ route('product-create') }}">Insert a Product</a>

    <table class="table table-secondary table-striped mt-5">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Date</th>
                <th scope="col">Handle</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($products as $product)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ \Carbon\Carbon::parse($product->created_at)->format('d/m/Y') }}</td>
                    <td>
                        <a href="" id="edit{{ $product->id }}" class="btn btn-primary">Edit</a>

                        <button class="btn btn-danger delete-product"
                            data-product-id="{{ $product->id }}">Delete</button>

                    </td>
                </tr>
            @endforeach



        </tbody>
    </table>
    <script>
        $(document).on('click', '.delete-product', function() {
            var productId = $(this).data('product-id');
            var url = "{{ route('product.destroy', ':id') }}?t=" + new Date().getTime();
            url = url.replace(':id', productId);
            $.ajax({
                type: 'DELETE',
                url: url,
                cache: false,
                data: {
                    _token: '{{ csrf_token() }}'
                },
                success: function(response) {
                    $('.product-' + productId).remove();
                    window.location = '/'
                    alert(response.message);
                },
                error: function(response) {
                    alert("Error deleting product");
                }
            });

        });
    </script>
</body>

</html>
