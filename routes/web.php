<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProductController::class,'index'])->name('index');
Route::get('/create', [ProductController::class,'create'])->name('product-create');
Route::delete('/product/{product}', [ProductController::class,'destroy'])->name('product.destroy');



Route::get('/get-data', [ProductController::class,'getData'])->name('get-data');
Route::post('/insert-data', [ProductController::class,'store'])->name('insert-data');
