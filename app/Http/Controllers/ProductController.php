<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Facade\FlareClient\Http\Response;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function getData()
    {
        $datas = Product::all();
        return view('products.index', compact('datas'));
    }
    public function insertData(Request $request)
    {
       
    }   




    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->get();
        return view('products.index', compact('products'));
    }

    public function store(Request $request)
    {
        try {
            $data = $request->all();
            Product::create($data);
            return response()->json(['result' => 'Successfully Inserted Data'],200);
            
        } catch (QueryException $e) {
            return response()->json(['result' => $e],200);
        }
    }

    public function update(Request $request, $id)
    {
        
    }

    public function create()
    {
        return view('products.add');
    }

   
    
    public function show(Product $product)
    {
        //
    }

    
    public function edit(Product $product)
    {
        //
    }

  


   
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->json(['result' => 'Successfully Deleted Data'],200);
    }
}
